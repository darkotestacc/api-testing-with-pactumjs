const pactum = require('pactum');
const { Given, When, Then, Before, After } = require('@cucumber/cucumber');

let spec = pactum.spec();

Before(() => {
  spec = pactum.spec();
});


/*-------------Make any request to any endpoint-------------*/
Given(/^I make a (.*) request to (.*) endpoint$/, (method, endpoint) => {
    spec[method.toLowerCase()](endpoint);
  });

/*-------------Select a specific BIN by adding ID-------------*/
Given(/^I make a (.*) request for a BIN's with an ID value of (.*) from an endpoint (.*)$/, (method, binID, endpoint) => {
    spec[method.toLowerCase()](endpoint+'/'+binID)
    .withPathParams('project', 'project-name');
  });

/*-------------Select a specific BIN and the version-------------*/
Given(/^I make a (.*) request for a BIN's with an ID value of (.*) and it's version of (.*) from an endpoint (.*)$/, (method, binID, version, endpoint) => {
    spec[method.toLowerCase()](endpoint+'/'+binID+'/'+version)
    .withPathParams('project', 'project-name');
  });

/*-------------Use the auth token for the private bins-------------*/
Given(/^I set an auth token to be (.*)$/, (authToken) => {
    spec.withHeaders({
        'Content-Type': 'application/json',
        'X-Master-Key': authToken,
      })
  });

/*-------------Update a BIN with a new JSON-------------*/
Given('I set the BIN\'s JSON to be a new, updated one', () => {
    let number = Math.floor(Math.random() * 100) //just some random number, so I can keep track of the changes
    spec.withJson(
        {"new": "update"+number})
  });

/*-------------Set private BIN's name-------------*/
Given(/^I set the bin name to be (.*)$/, (binName) => {
    spec.withHeaders({
        'X-Bin-Name': binName
      })
  });

/*-------------Choose an existing Collection that will hold the BIN-------------*/
//bare in mind that the collection must be created prior to this, and you should have it's ID!
Given(/^I set the existing Collection's ID it to be (.*)$/, (collectionID) => {
    spec.withHeaders({
        'X-Collection-Id': collectionID
      })
  });

/*-------------Use a default JSON in the body of a request-------------*/
Given('I set a JSON bin to be default', () => {
    spec.withBody('{ "title": "something", "content": "something else"}')
  });
  

/*-------------Wait for a response-------------*/
When('I receive a response', async () => {
    await spec.toss();
  });



/*-------------Asertions of the response-------------*/


//Status code
Then(/^I expect response should have a status (.*)$/, (code) => {
    spec.expectStatus(code);
  });

//Full JSON 
Then(/^I expect response should have a json like (.*)$/, (expectedJSON) => {
    spec.jsonLike(JSON.parse(expectedJSON));
  });

//Partial values of JSON
Then(/^I expect response should contain (.*) with the value of (.*)$/, (path, value) => {
    spec.expectJsonLike(path, value);
  });


After(() => {
    spec.end();
  });

