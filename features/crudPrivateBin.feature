Feature: Testing of the CRUD functionalities for a Private bin

        Scenario: Create a private bin
            Given I make a POST request to https://api.jsonbin.io/v3/b endpoint
            And I set an auth token to be $2b$10$H0Zz/xT9idzsMGnW0GOrEupDoOTvvYd5jCq3Aq8f7hzMYTqyhjIAi
            And I set the bin name to be FirstOneCreated
            And I set a JSON bin to be default
            When I receive a response
            Then I expect response should have a status 200
            And I expect response should contain title under record with the value of something


        Scenario: Read an existing bin
            Given I make a GET request for a BIN's with an ID value of 61f0f9bdc37c95494355092e from an endpoint https://api.jsonbin.io/v3/b
            When I receive a response
            Then I expect response should contain sample with the value of 'to be read'


        Scenario: Update an existing bin
            Given I make a PUT request for a BIN's with an ID value of 61f10548bd6e744997ec0a71 from an endpoint https://api.jsonbin.io/v3/b
            And I set an auth token to be $2b$10$H0Zz/xT9idzsMGnW0GOrEupDoOTvvYd5jCq3Aq8f7hzMYTqyhjIAi
            And I set the BIN's JSON to be a new, updated one
            When I receive a response
            Then I expect response should have a status 200
            And I expect response should contain new with the value of update

        Scenario: Delete an existing bin (this is not a hard delete)
            Given I make a DELETE request for a BIN's with an ID value of 61f156a2c37c954943552d00 from an endpoint https://api.jsonbin.io/v3/b
            When I receive a response
            Then I expect response should have a status 200
            And I expect response should contain message under record with the value of 'Bin deleted successfully'

        Scenario: [for the future work/ideas] Create a bin inside of an existing collection
            Given I make a POST request to https://api.jsonbin.io/v3/b endpoint
            And I set an auth token to be $2b$10$H0Zz/xT9idzsMGnW0GOrEupDoOTvvYd5jCq3Aq8f7hzMYTqyhjIAi
            And I set the existing Collection's ID it to be 61f158dec37c954943552e96
            And I set the bin name to be BinInTheCollection
            And I set a JSON bin to be default
            When I receive a response
            Then I expect response should have a status 200
            And I expect response should contain title under record with the value of something

