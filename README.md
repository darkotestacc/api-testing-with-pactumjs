## Preconditions and how to run the project: 

1. Install Node.js v15.11.0 or above 
2. Clone this project to your machine: 
```bash
git clone https://gitlab.com/darkotestacc/api-testing-with-pactumjs.git
```
3. cd into the cloned repo 
```bash
cd api-testing-with-pactumjs
```
4. install the node_modules
```bash
npm install
```
5. Run the following command to start the tests: 
```bash
npm test 
```


## Reports and data manipulation: 

When you run the tests in the terminal, you will have default results diplayed in the output.

In case you want to change or update some test data, you can do it in the: features/crudPrivateBin.feature
